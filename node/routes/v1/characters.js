var express = require("express");
var router = express.Router({ mergeParams: true });
const axios = require("axios").default;

module.exports = router;

const auth = require("../../auth/auth");

/* GET users listing. */
router.get("/", auth.authenticate, function (req, res, next) {
  //res.send('respond with a resource');

  var http_response = {
    success: false,
    message: "unknown_error",
  };

  if (!req.params.id) {
    http_response.message = "no_id";
    res.json(http_response);
  }

  const get_film = async (film_id) => {
    let url = `https://swapi.dev/api/films/${film_id}/`;
  
    try {
      const film = await axios.get(url);
      film.data.characters = await get_characters(film.data.characters);
  
      http_response.success = true;
      http_response.data = film.data.characters;
      delete http_response.message;

      
      
    } catch (error) 
    {
      http_response.message = error.message
    }
    res.json(http_response);

  };
  
  const get_characters = async (arr) => {
    var characters = arr.map(async (url) => {
      const character = (await axios.get(url)).data;
      const origin = (await axios.get(character.homeworld)).data;
  
      var species = ['Human'];
  
      if(character.species && character.species.length)
      {
        species = character.species.map(async (url) => 
        {
          const sp = (await axios.get(url)).data;
          return sp.name;  
        });
      }
  
      return {
        name: character.name,
        gender: character.gender,
        species: await Promise.all(species),
        origin: origin.name
      };
  
      
    });
  
    return await Promise.all(characters);
  };

  get_film(req.params.id);
});

