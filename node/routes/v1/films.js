var express = require("express");
var router = express.Router();
const axios = require("axios").default;


module.exports = router;

const auth = require("../../auth/auth");
var jwt = require("jsonwebtoken");




/* GET users listing. */
router.get("/", auth.authenticate ,function (req, res, next) 
{
  //res.send('respond with a resource');

  var http_response = {
    success: false,
    message: "unknown_error",
  };


  var token = req.headers['authorization']
  if(!token)
  {
      http_response.message = "no_token";
  }
  
  token = token.replace('Bearer ', '')

  jwt.verify(token, process.env.JWT_TOKEN, function(err, user) {
    if (err) 
    {
      http_response.message = "invalid_token"
    }
    else
    {
      http_response.success = true;
    }
  });

  if(!http_response.success)
  {
    res.json(http_response);
    return;
  }


  axios
    .get("https://swapi.dev/api/films/")
    .then(function (response) {
      // handle success
      console.log(response);

      var errmsg = "";

      var movies = response.data.results.map(function (m) {
        movie = {
          id: m.episode_id,
          title: m.title,
        };

        return movie;
      });

      http_response.success = true;
      http_response.data = movies;
      delete http_response.message;
    })
    .catch(function (error) {
      // handle error

      http_response.success = false;
      http_response.message = error.message;
    })
    .then(function () {
      res.json(http_response);
    });
});
