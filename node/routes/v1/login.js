var express = require("express");
var router = express.Router();

module.exports = router;
const bcrypt = require("bcryptjs");
var jwt = require("jsonwebtoken");

/* GET users listing. */
router.post("/", function (req, res, next) {
  //res.send('respond with a resource');

  

  var http_response = {
    success: false,
    message: "unknown_error",
  };


  http_response.success = (bcrypt.compareSync(req.body.email, process.env.AUTH_EMAIL_HASH) && bcrypt.compareSync(req.body.password, process.env.AUTH_PASS_HASH));

  if (http_response.success) {
    var tokenData = {
      email: req.body.email,
    };

    var token = jwt.sign(tokenData, process.env.JWT_TOKEN, {
      expiresIn: 60 * 60 * 24 * 7,
    });

    delete http_response.message
    http_response.token = token;

  } else {
    http_response.message = "no_login";
  }

  res.json(http_response);
});
