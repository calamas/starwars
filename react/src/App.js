import logo from './logo.svg';
import './App.css';

import Login from "./components/login";
import Films from "./components/films";

var user = null;

function App() 
{

  return (
    <div className="App">
      <Login/>

      <Films/>
      
    </div>
  );
}

export default App;
