import React from "react";

import axios from "axios";

import { Form } from "react-bootstrap";
import { Button } from "react-bootstrap";


export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "Please write an essay about your favorite DOM element.",
    };

    this.handleSubmit = this.handleSubmit.bind(this);

    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);

    this.state = {
        email: "",
        password: ""
      };
  }

  onChangeEmail(e) {
    this.setState({
      email: e.target.value
    });
  }

  onChangePassword(e) {
    this.setState({
      password: e.target.value
    });
  }

  handleSubmit(event) {
    
    event.preventDefault();


    return axios.post('http://localhost:1234/v1/login', {
        email: this.state.email,
        password: this.state.password
      }).then(response => {
        if (response.data.success) 
        {
          //localStorage.setItem("user", JSON.stringify(response.data));
          localStorage.token = response.data.token;
          alert("Auntentificado, ahora puedes buscar peliculas")
        }
        else
        {
          alert("Error")
        }

        return response.data;
      }).catch(function (error) {
        // handle error
  
        alert(error.message);
      })
      ;
  }

  render() {
    return (
      <div className="row">
        <div className="col-md-4 col-md-offset-4">
          <div className="card card-container">
            <Form onSubmit={this.handleSubmit}
            ref={c => {
              this.form = c;
            }}> 
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" placeholder="Ingresar email" name="email" value={this.state.email}
                onChange={this.onChangeEmail}/>
                
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Ingresar pass" name="password" value={this.state.password}
                onChange={this.onChangePassword}/>

               
              </Form.Group>
              <Button variant="primary" type="submit">
                Submit
              </Button>
            </Form>
          </div>
        </div>
      </div>
    );
  }
}
