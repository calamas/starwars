import React from "react";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";

import axios from "axios";

export default class Films extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "Please write an essay about your favorite DOM element.",
    };

    this.handleClick = this.handleClick.bind(this);
    this.handleClick2 = this.handleClick2.bind(this);
  }

  films = [];
  characters = [];

  handleClick(event) {
    if (!localStorage.token) {
      alert("No estas autentificado");
      return;
    }

    var token = "Bearer " + localStorage.token;

    return axios
      .get("http://localhost:1234/v1/films", {
        headers: {
          Authorization: token,
        },
      })
      .then((response) => {
        if (response.data.success) {
          this.films = response.data.data;
          //alert(JSON.stringify(this.films))
          this.setState({
            films: this.films,
            filmsLoading: false,
          });
        } else {
        }

        return response.data;
      })
      .catch(function (error) {
        // handle error

        alert(error.message);
      });
  }

  handleClick2(event) {
    if (!localStorage.token) {
      alert("No estas autentificado");
      return;
    }

    var token = "Bearer " + localStorage.token;
    
    return axios
      .get("http://localhost:1234/v1/films/" + event.target.value + "/characters", {
        headers: {
          Authorization: token,
        },
      })
      .then((response) => {
        if (response.data.success) {
          this.characters = response.data.data;

          this.setState({
            characters: this.characters,
            films: this.films,
            filmsLoading: false,
          });
        } else {
        }

        return response.data;
      })
      .catch(function (error) {
        // handle error

        alert(error.message);
      });
  }

  componentDidMount() {
    this.setState({});
  }

  render() {
    return (
      <Container>
        <Row>
          <Col>
            <Button onClick={this.handleClick}>Buscar Peliculas</Button>
          </Col>
        </Row>
        <Row>
          {this.films.map((item, index) => (
            <Col key={index}>
              <Button value={item.id} onClick={this.handleClick2}>{item.title}</Button>
            </Col>
          ))}
        </Row>

        <Row>
          <Col>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Sexo</th>
                  <th>Origen</th>
                  <th>Especie</th>
                </tr>
              </thead>
              <tbody>
                {this.characters.map((item, index) => (
                  
                    <tr key={index}> 
                      <td>{item.name}</td>
                      <td>{item.gender}</td>
                      <td>{item.origin}</td>
                      <td>{item.species.join(',')}</td>
                    </tr>
                  
                ))}
              </tbody>
            </Table>
          </Col>
        </Row>
      </Container>
    );
  }
}
